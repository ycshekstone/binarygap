function solution(N) {

    // store the length of longest consecutive zero
    let maxLenZ = 0;

    // store the length of current consecutive zero
    let numZ = 0;

    // determine when to start counting the consecutive zero
    let cntSt = false;

    while (N > 0)
    {
        if ((N & 1) == 1)
        {
            cntSt = true
            maxLenZ = Math.max(maxLenZ, numZ);
            numZ = 0;
        }
        else if (cntSt)
        {
            numZ++;
            maxLenZ = Math.max(maxLenZ, numZ);
        }
        N >>= 1;
    }

    return maxLenZ;
};

console.log(solution(1041));